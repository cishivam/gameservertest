const rank = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
const suit = ["Heart", "Diamonds", "Clubs", "Spades"];
const card = { suit: "", rank: "" };
const Deck = [];
for (let i = 0; i < rank.length; i++) {
  for (let j = 0; j < suit.length; j++) {
    Deck.push({
      ...card,
      rank: rank[i],
      suit: suit[j],
    });
  }
}
function checkFlush(cardsArr) {
  let checkPoint = cardsArr[0].suit;
  for (let i = 1; i < cardsArr.length; i++) {
    if (checkPoint !== cardsArr[i].suit) {
      console.log("notFlush");
      return "notFlush";
    }
  }
  console.log("flush");
  return "flush";
}
function checkStraight(cards) {
  var foundIndexs = [];
  for (let i = 0; i < cards.length; i++) {
    rank.map((item, index) => {
      if (item === cards[i].rank) {
        foundIndexs.push(index);
      }
    });
  }
  var sorted = foundIndexs.sort((a, b) => a - b);

  let newArr = sorted.concat(sorted);
  let startIndex;
  let result = "straight";
  for (let i = 0; i < newArr.length / 2; i++) {
    if (newArr[i + 1] - newArr[i] === 1) {
      startIndex = i;
      break;
    } else startIndex = 0;
  }
  for (let i = startIndex; i < newArr.length / 2 + startIndex - 1; i++) {
    if (newArr[i + 1] - newArr[i] !== 1 && newArr[i] - newArr[i + 1] !== 12) {
      result = "not consecutive";
      return result;
    }
  }
  console.log(result, "<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>");
  return result;
}
function doShuffle(n) {
  for (let i = 0; i <= n; i++) {
    var j = Math.floor(Math.floor(Math.random() * 100) / 2);

    var k = Math.floor(Math.floor(Math.random() * 100) / 2);

    let temp = Deck[j];
    Deck[j] = Deck[k];
    Deck[k] = temp;
  }
  console.log(Deck);
  return Deck;
}
// to run this file simply type node cardGame.js in same directory 
//////// to run suffle uncomment code below ,pass n = how many times you want to shuffle
// doShuffle(50);
/////// to run checkFlush uncomment code below
// checkFlush([
//   { suit: "Heart", rank: "5" },
//   { suit: "Heart", rank: "5" },

//   { suit: "Heart", rank: "5" },
//   { suit: "Heart", rank: "5" },
//   { suit: "Heart", rank: "5" },
//   { suit: "Heart", rank: "5" },

//   { suit: "Heart", rank: "5" },
// ]);
//to run CheckStraight uncomment code below
// checkStraight([
//   { suit: "Heart", rank: "A" },
//   { suit: "Spades", rank: "Q" },
//   { suit: "Heart", rank: "J" },
//   { suit: "Diamonds", rank: "K" },
//   { suit: "Diamonds", rank: "8" },
//   { suit: "Diamonds", rank: "7" },
//   { suit: "Diamonds", rank: "9" },
//   { suit: "Diamonds", rank: "10" },
//   { suit: "Diamonds", rank: "5" },
//   { suit: "Diamonds", rank: "6" },
// ]);

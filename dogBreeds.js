async function dogBreeds() {
  const axios = require("axios");
  const fs = require("fs");
  let breeds;
  try {
    breeds = await axios.get("https://dog.ceo/api/breeds/list/all");
  } catch (err) {
    console.log(err);
  }
  let breedList = Object.keys(breeds.data.message);
  for (let i = 0; i < 3; i++) {
    let randomIndex = Math.floor(Math.random() * (breedList.length + 1));
    console.log(breedList[randomIndex]);
    axios
      .get(`https://dog.ceo/api/breed/${breedList[randomIndex]}/images`)
      .then((res) => {
        console.log(res.data.message);
        let randomImage = Math.floor(
          Math.random() * (res.data.message.length + 1)
        );
        fs.appendFileSync(
          "Output.txt",
          `${res.data.message[randomImage]} (${breedList[randomIndex]})\n`,
          "utf8"
        );
      })
      .catch((err) => err);
  }
}
dogBreeds();
// before runnig this code please run npm install in same directory
// to run this code simple in the same folder type :- node dogBreeds.js
// output will be stored in output.txt file